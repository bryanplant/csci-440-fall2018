#!/usr/bin/env python

import platform
import sqlite3

DB_FILE = 'tmp.db'

NOOP, SHOW_NAMES, SHOW_FRIENDS, ADD_FRIEND, SHOW_USER_BY_NAME, \
        SHOW_USER_BY_ORDER, FRIEND_OR_LIKES, QUIT = range(8)


def get_input(prompt):
    py_version = platform.python_version()
    return raw_input(prompt) if py_version.startswith('2') else input(prompt)


def user_input():
    action = NOOP
    while int(action) not in (SHOW_NAMES, SHOW_FRIENDS, ADD_FRIEND,
            SHOW_USER_BY_NAME, SHOW_USER_BY_ORDER,
            FRIEND_OR_LIKES, QUIT):
        print('What would you like to do?')
        print('1. Show Names')
        print('2. Show Friends')
        print('3. Add Friend')
        print('4. Show User by name')
        print('5. Show User by order num')
        print('6. Friends or Likes')
        print('7. Quit')
        action = int(input('> '))
    return action


def show_names(conn):
    c = conn.cursor()
    c.execute('SELECT name FROM Highschoolers ORDER BY name')
    for row in c :
        print(row[0])
    conn.commit()


def show_friends(conn):
    name = get_input('Name? > ')
    c = conn.cursor()
    sql = """
        SELECT H2.name
        FROM Highschoolers as H1
            inner join Friends on H1.ssn = Friends.ssn1
            inner join Highschoolers as H2 on H2.ssn = Friends.ssn2
        where H1.name = '{}'
    """.format(name)
    print(sql)
    c.execute(sql)
    for row in c.fetchall():
        print(row[0])
    conn.commit()

def show_user_by_name(conn):
    name = get_input('Name? > ')
    c = conn.cursor()
    sql = """
        SELECT *
        FROM Highschoolers
        where name = '{}'
    """.format(name)
    print(sql)
    c.execute(sql)
    conn.commit()
    for row in c.fetchall():
        print(row)

def show_user_by_order(conn):
    val = get_input('Ordering Val? > ')
    c = conn.cursor()
    sql = """
        SELECT *
        FROM Highschoolers
        where ordering = {}
    """.format(val)
    print(sql)
    c.execute(sql)
    conn.commit()
    for row in c.fetchall():
        print(row)


def show_friends_or_likes(conn):
    name = get_input('Name? > ')
    friend_or_likes = get_input('friends or likes?> ')
    c = conn.cursor()
    sql = """
        SELECT *
        FROM Highschoolers as H1
            inner join {} as K on H1.ssn = K.ssn1
        where H1.name = '{}'
    """.format(friend_or_likes, name)
    print(sql)
    c.execute(sql)
    conn.commit()
    for row in c.fetchall():
        print(row)


def add_friend(conn):
    name1 = get_input('Name1? > ')
    name2 = get_input('Name2? > ')


    c = conn.cursor()
    before_count = c.execute('SELECT count(*) from Friends').fetchone()[0]

    sql = """
        INSERT INTO Friends VALUES
            (
                (SELECT ssn FROM Highschoolers WHERE name = '{}'),
                (SELECT ssn FROM Highschoolers WHERE name = '{}')
            )
        """.format(name1, name2)
    c.executescript(sql)
    after_count =  c.execute('SELECT count(*) from Friends').fetchone()[0]
    conn.commit()
    print("Success" if before_count != after_count else "Fail")


def process_action(conn, action):
    print(action)
    if action == SHOW_NAMES:
        show_names(conn)
    elif action == SHOW_FRIENDS:
        show_friends(conn)
    elif action == ADD_FRIEND:
        add_friend(conn)
    elif action == SHOW_USER_BY_NAME:
        show_user_by_name(conn)
    elif action == SHOW_USER_BY_ORDER:
        show_user_by_order(conn)
    elif action == FRIEND_OR_LIKES:
        show_friends_or_likes(conn)

def check_version():
    py_version = platform.python_version()
    if not py_version.startswith('2'):
        raise NotImplementedError('\n'
                + '####################################################\n'
                + 'ERROR: Example only supports Python version 2.X\n'
                + 'You are running {}\n'.format(py_version)
                + '####################################################\n'
                + '\n'
        )


def main():
    check_version()
    conn = sqlite3.connect(DB_FILE)

    action = user_input()
    while action != QUIT:
        process_action(conn, action)
        action = user_input()


if __name__ == '__main__':
    main()
