# Transactions

Sometimes we may need to perform multiple actions together or multiple users may
be using the system and we don't want to have to consider the complexities for
updating the data at the same time.  Transactions allow for a simple interface
for a database user to solve both problems.

One common example of why this would occur is in a banking system.  Image that
we had users account balances.

    drop table if exists Savings;

    create table Savings
        id text primary key,
        balance real
    );

    insert into Savings
    values ('a', 1), ('b', 2), ('c', 3), ('d', 5), ('e', 8);

    -- transfer 1 dollar from d to e
    update Savings set balance = balance + 1 where id = 'd';
    update Savings set balance = balance - 1 where id = 'e';

    select * from Savings;

What problem could occur? Well, if we consider the lines:

    update Savings set balance = balance + 1 where id = 'd';
    update Savings set balance = balance - 1 where id = 'e';

What happens if the compute is turned off after the first line?  We somehow
ended up with an extra dollar.  That would be bad for business.  So, we either
want both operations to fail or both to succeed.  In fact there is a nice
acronym describing the properties.

## ACID

ACID is an acronym for the properties of transactions defined by [Haerder and
Reuter](https://dl.acm.org/citation.cfm?doid=289.291).  We can summarize the
ideas as:

* Atomicity: all operations in the transaction are successful or unsuccessful.
  E.g. If there are three operations and the second failed, the system will
  return to a state where the first operation never happened.

* Consistency: database moves from one valid state to another valid state.  E.g.
  No constraints are violated before the transaction and none are violated
  after.

* Isolation: if multiple transactions are occurring at the same time, the system
  will behave as if the transactions occurred sequentially.  E.g. If I make a
  change and you make a change at the same time, the system will behave as if
  you made all your changes and then I made all my changes or I made all my
  changes and you made all your changes but our changes will not be intertwined.

* Durability: Once the transaction is committed it will remain committed.  E.g.
  if the system crashes the transaction will not have disappeared.

## Using Transactions

We can use transactions to solve our problem of transferring money by executing
in a transaction.  The transaction syntax is:

    BEGIN TRANSACTION;
        -- commands evaluated in the transaction;
    COMMIT;

For example, we solve the problem with:

    BEGIN TRANSACTION;
        update Savings set balance = balance + 1 where id = 'd';
        update Savings set balance = balance - 1 where id = 'e';
    COMMIT;

Lets see the transaction in action:

    $ sqlite3 tmp.db
    > select * from Savings;
    id          balance
    ----------  ----------
    a           1.0
    b           2.0
    c           3.0
    d           5.0
    e           8.0
    > BEGIN TRANSACTION;
    > update Savings set balance = balance + 1 where id = 'd';
    > select * from Savings;
    id          balance
    ----------  ----------
    a           1.0
    b           2.0
    c           3.0
    d           6.0
    e           8.0

Observe that the 'd' was updated.  Now, lets NOT commit, but quit the shell,
start it again and see what happens

    > .quit
    > sqlite3 tmp.db
    > select * from Savings;
    id          balance
    ----------  ----------
    a           1.0
    b           2.0
    c           3.0
    d           5.0
    e           8.0

And observe the changes were reverted.

In fact we can revert by issuing the `ROLLBACK` command.

    > BEGIN;
    > update Savings set balance = balance + 1 where id = 'd';
    > select * from Savings;
    id          balance
    ----------  ----------
    a           1.0
    b           2.0
    c           3.0
    d           6.0
    e           8.0
    > ROLLBACK;
    > select * from Savings;
    id          balance
    ----------  ----------
    a           1.0
    b           2.0
    c           3.0
    d           5.0
    e           8.0

