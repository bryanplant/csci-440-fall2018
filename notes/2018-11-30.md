# MongoDB Pt 2

One of the great parts about MongoDB's use of javascript is that the
programming model is a little more natural than with SQL.  Lets see an example
of how we can use functions to output the results of a query and insert data.

Lets add some data, make a query and print out

    // cleanup from ;previous runs
    db.musicians.drop()
    db.lables.drop()

    // create a few helper functions
    function printQueryResult(tiktle, cursor) {
      print(`#################${title}#######################`);
      while(cursor.hasNext()) {
        printjsononeline(cursor.next());
      }
      print("\n");
    }

    function insertLabel(name) {
      db.lables.insert({name: name});
    }

    function insertMusician(name, instrument) {
      db.musicians.insert({name: name, instrument: instrument});
    }


    // insert some musicians
    insertMusician("Elvin Jones", ["drums"]);
    insertMusician("McCoy Tyner", ["piano"]);
    insertMusician("Jimmy Garrison", ["bass"]);
    db.musicians.insert({
      name: "John Coltrane",
      instrument: ["tenor sax", "soprano sax", "flute"],
      albums: [
        { name: "blue trane", year: 1958 },
        { name: "giant steps", year: 1960 },
        { name: "Both Directions At Once", year: 2018 }
      ]
    });

    // insert some labels
    insertLabel("impulse");
    insertLabel("electra");
    insertLabel("geffin");

    // Do some searches
    var cursor = db.musicians.find();
    printQueryResult("MUSICIANS", cursor);

    var cursor = db.lables.find();
    printQueryResult("LABLES", cursor);

Lets try to find records.

First, lets find by id

    > db.musicians.find( { "_id": ObjectId(<An objec id>)});

Or name

    > db.musicians.find( { name: "Elvin Jones" });

Or parameter

    > db.musicians.find( { name: /^E/ });

... In fact, we were using a shortcut.  When we call find, the object we pass
defines the query.  What we were really doing is issuing a query that returned
all documents in which the `name` was "Elvin Jones".  The full query is written
as:

    > db.musicians.find( { name: { $eq: "Elvin Jones" });

We can also search by ranges:

    > db.musicians.find( { birth_year: { $gt: 1929, $lt: 1937 } });

And just like the relational model, we can do projections

    printQueryResult("Projecting",
      db.musicians.find(
        { birth_year: { $gt: 1929, $lt: 1937 } },
        { _id: 0, name: 1, }
      )
    )

Or existential queries such as "all musicians that play piano":

    > db.musicians.find( { instrument: { $all: ["piano"] } }));

But we can also go deep into the structure of documents to find queries.  Lets
explore a few example from a great book [Seven Databases in Seven Weeks Volume
2]( https://pragprog.com/book/rwdata/seven-databases-in-seven-weeks).

    > db.countries.insert({
        _id : 'us',
        name : 'United States',
        exports : {
          foods : [
            { name : 'bacon', tasty : true },
            { name : 'burgers' }
          ]
        }
      })
    > db.countries.insert({
        _id : 'ca',
        name : 'Canada',
        exports : {
          foods : [
            { name : 'bacon', tasty : false },
            { name : 'syrup', tasty : true }
          ]
        }
      })
    > db.countries.insert({
        _id : 'mx',
        name : 'Mexico',
        exports: {
          foods : [{
            name : 'salsa',
            tasty: true,
            condiment: true
          }]
        }
    })

Now lets find all countries that export bacon.

    db.countries.find({
        'exports.foods.name' : { $eq: 'bacon' }
    })

Note that we put the nested as a seq connected with `.`. Also, recall that we
can shorten up the command as

    db.countries.find({
        'exports.foods.name' : 'bacon'
    })

Now, lets find tasty bacon:

    db.countries.find({
        'exports.foods.name' : 'bacon',
        'exports.foods.tasty' : true,
    })

.... hummm that seems wrong.  Or data lists bacon from Canada as not tasty.
Why?

... well, the Canada document does contain something where tasty is true, so it
satisfies the query.  What we need is a single element with both properties
matching.  We can use `$elemMatch` to say that we want one element that matches.

    db.countries.find({
      'exports.foods' : {
        $elemMatch : {
          name : 'bacon',
          tasty : true,
        }
      }
    })

And now, we only get a match for the United States document.

Just like we were implicitly doing `$eq` queries, we have also been implicitly
performing queries with an AND.  For example if we wanted to find counties with bacon
and burgers we could write:

    db.countries.find({
      'exports.foods.name' : 'bacon',
      'exports.foods.name' : 'burgers'
    })

But what if we wanted to burgers or salsa?

    db.countries.find({
      $or : [
        { 'exports.foods.name' : 'burgers' },
        { 'exports.foods.name' : 'salsa' }
      ]
    }, { _id: 1 })

# Now you try (QUIZ)

For this class you added a few collections and documents.

1. What is the name of the collections that you added?
2. Did you have any documents in the same collection with differing structure?
3.  Use `find()` to make queries on your data using `$eq, $lt, $gt, $elemMatch,
    $or` and other searching tools described in the [MongoDB Find
    Docs](https://docs.mongodb.com/manual/reference/method/db.collection.find/)
