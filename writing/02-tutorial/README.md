# How to draw an ER diagram: Group number - 13

The objective of this tutorial will be learning the steps for creating an ER diagram. This will include how to identify Entities, Attributes, and relationships, as well as how to properly represent them in a diagram. ER diagrams are very important to the initial design process of a database. Knowing how to visually represent data is instrumental for communicating ideas with other team members in the beginning stages of a complicated project. The value of this tutorial will be learning how to communicate and visually represent ideas in an organized and efficient way. A detailed index of this tutorial can be found below.

1. What is an ER Diagram?  
2. Why is diagramming important?  
3. ER Diagram background information  
3.1 Entities  
3.1.1 Strong Entities  
3.1.2 Weak Entities  
3.2 Attributes  
3.2.1 Composite and Atomic Attributes  
3.2.2 Derived Attributes  
3.2.3 Single-valued Attributes  
3.2.4 Multi-valued Attributes  
3.3 Keys  
3.3.1 Primary Keys  
3.3.2 Composite Keys  
3.3.3 Partial Keys  
3.3.4 Candidate Keys  
3.4 Value Sets  
3.5 Relationships  
3.5.1 Relationship Set  
3.5.2 Mapping Cardinalities  
3.6 Participation  
1. Bringing it all together  
4.1 Identifying entities and attributes  
4.2 Identifying types of entities and attributes  
4.3 Identifying relationships  
4.4 Putting it all together  

## 1. What is an ER Diagram?

An ER diagram is based off the Entity-Relationship (ER) model, a description of a database conceptual view. It shows the complete logical structure of the discrete components of a database and how they relate to one another. In designing a database, an ER diagram is crucial to fully understand and develop a functional and useful system. Note that this tutorial will not cover the ER model, but it can be useful to know how that works before attempting to design an ER diagram. 

## 2. Why is diagramming important?

Diagramming is an important step in designing a database with specific needs. Taking the time to draw out the design forces programmers to take a step back and think about what the data needs to represent and how to organize it well. It also allows them to think about what the big picture is and the main problem that needs to be solved. Without taking that step back it is easy to get caught up in the little things and end up with a less than optimal solution. 
	
Not only is diagramming important for the programmers but it is also important for the project as a whole. Rarely will there ever be a project where the only relevant members of the team are programmers that understand all the concepts talked about so far. More likely there are going to be nontechnical people involved with the project and design that will not understand the code or database structure on a level that programmers do. It provides a simplified visual that is easy to understand by anyone.

## 3.  ER diagram background information

Before we can draw an ER diagram we must first identify the components of the database. The following sections describe these components and how they are used in an ER diagram.

### 3.1 Entities
An entity is a thing or object in the real world with an independent existence. An entity can be a physical object like a person or a building, an event, or a concept. Entities are related to each other by relationships and described by attributes. There are two types of entities: strong and weak.

#### 3.1.1 Strong Entities
A strong entity is an independent entity which does not depend on the existence of another entity.. A strong entity is represented in an ER diagram as a rectangle with the name of the entity in the middle of the rectangle, shown in the figure below.

![Figure 3.1](images/1.png)  
*Figure 3.1*

#### 3.1.2 Weak Entities
A weak entity is any entity that depends on the existence of a strong entity. A weak entity is represented with a double rectangle. Every weak entity must be related to a strong entity and have a partial key attribute as seen below. Attributes will be expanded on next.

![Figure 3.2](images/2.png)  
*Figure 3.2*

### 3.2 Attributes
An attribute is a property that describes an entity. There are multiple types of attributes: composite, atomic, single-valued and multivalued. Attributes are represented as ovals with the name of the attribute in the middle. In an ER diagram attributes are connected to entities with a solid line. 

![Figure 3.3](images/3.png)  
*Figure 3.3*

#### 3.2.1 Composite and Atomic Attributes
A composite attribute is an attribute that is divisible into subparts with independent meaning. An atomic attribute cannot be divided and can be referred to as a simple attribute. An example of a composite attribute is an address because it can be divided into atomic attributes such as number, street name, and zip code. 

Instead on an oval attached to an entity a composite attribute is an oval attached to another oval with a solid line. An atomic attribute is represented as explained in the previous section.

![Figure 3.4](images/4.png)  
*Figure 3.4*

#### 3.2.2 Derived Attributes
A derived attribute is an attribute that can be calculated or found by using other values stored in the database. Age is an example if a derived attribute because it can be calculated using birth year and current year. A derived attribute is represented as an oval with a dashed line, as seen below.

![Figure 3.5](images/5.png)  
*Figure 3.5*

#### 3.2.3 Single-valued Attributes
A single-valued attribute is an attribute that can only have a single value at a particular instance of time, such as the age of a person. 

#### 3.2.4 Multi-valued Attributes
A multivalued attribute can have multiple values at one time, such as the colors of a shirt. Multivalued attributes are represented as a double oval with the name of the attribute in the middle of the inner oval, as seen in the figure below.

![Figure 3.6](images/6.png)  
*Figure 3.6*

### 3.3 Keys
Keys are one or more attributes whose values are distinct to an entity and are used to uniquely identify an entity. Keys should be minimal meaning no key attribute can be removed without causing the key to be non-unique.

#### 3.3.1 Primary Keys
A unique key that serves as an identifier for an entity. A primary key is represented in an ER diagram as a attribute where the name of the attribute is underlined. In Figure 3.7 below, Name is the primary key.

![Figure 3.7](images/7.png)  
*Figure 3.7*

#### 3.3.2 Composite Keys
Keys can be formed from one attribute, such as an SSN. Keys can also be made of multiple attributes, such as the combination of street name, number, and zip code. Keys that are made of more than one attribute are called composite keys.

#### 3.3.3 Partial Keys
In a composite key, the separate attributes forming the entire key are called partial keys. Partial keys also serve as a sort of primary key for weak entities, and tie the weak entities to their corresponding strong entity. This is represented in Figure 3.8.

![Figure 3.8](images/8.png)  
*Figure 3.8*

#### 3.3.4 Candidate Keys
Candidate keys are a set of keys for an entity. Generally these are attributes that the designer can pick between to be the primary key. Not every candidate key will end up being a primary key.

### 3.4 Value Sets
A value set is the set of all possible values for a simple attribute. For example, if the attribute is the alphabet then the value set would be the characters a - z. 

### 3.5 Relationships
A relationship is an association between entities. For example, an employee might work for a department. Similar to entities, relationships can have attributes to further describe them. 

#### 3.5.1 Relationship set
A relationship set is the collection of all relationships of the same type in a database. 

#### 3.5.2 Mapping Cardinalities
Another important concept about relationships is cardinality constraints. These constraints represent the maximum number of relationship instances an entity can participate in. These generally fall into three categories: 1:1 (read one to one), 1:N (read one to N), and M:N (read M to N). These are explained further below.

_One-to-one_  
As shown in Figure 3.9, an Employee can manage one Department and a Department can have one Employee managing it. This can be changed based on the type of situation that is being modeled. In an ER diagram relationships are represented as a diamond with the name of the relationship in the middle. The diamond is also connected to all entities involved in the relationship with a solid line. 

When a relationship has a one to one association a one is placed above each of the solid lines connecting the relationship to its entities. Figure 3.9 also has the relationship connecting Manages to Department with two lines - this will be explained later.

![Figure 3.9](images/9.png)  
*Figure 3.9*

_One-to-many_  
When the relationship has a many to one relationship there in an N placed above the connect with multiple instances and a one is place above the connection with one instance, as seen below.

![Figure 3.10](images/10.png)  
*Figure 3.10*

_Many-to-many_  
When the relationship is many to many, an N is placed above both connections to entities. 

![Figure 3.11](images/11.png)  
*Figure 3.11*

### 3.6 Participation
In addition to cardinality constraints, relationships also have participation constraints. There are two types of participation constraints: total participation and partial participation.

_Total participation_  
If an entity has total participation in a relationship, every entity of that type must participate in the relationship. For example, an Employee must work for a Department. Again, this depends on the design.

![Figure 3.12](images/12.png)  
*Figure 3.12*

_Partial participation_  
In a partial participation relationship, only some entities participate. For example, Figure 3.13 shows a situation in which Employees do not necessarily have to have Dependents, so they partially participate in the relationship. However, the Dependent is a weak entity and therefore must fully participate in the relationship with Employee, as Employee is the corresponding strong entity.

![Figure 3.13](images/13.png)  
*Figure 3.13*


## 4. Bringing it all together

After reviewing the concepts above, let's look at an example of how to apply this information to an actual database of a company. The first step is to define all of the components of the ER diagram of our database. To do this we need to look at any and all information we know about our database going into the design process. The assumptions we can make for the Company example that will be used throughout the next few sections are described in Table 4.1 below.

*Table 4.1: Description of company database*

Assumptions | DEPARTMENTS | PROJECTS | EMPLOYEES
------------ | ------------- | ------------ | -------------
Departments control projects | Unique name | Unique name | name | 
Each employee is a part of only one department | Unique ID | Unique number | Social Security number
Each employee can work on several projects (projects may be from other departments) | One manager | One location | address
The number of hours spent each week working on a project is tracked | Date manager started | | salary
Each dependent has: First name, Gender, Birthdate, Relationship | Location (a department may have many locations) | | gender
| | | Birth date 
| | | supervisor
| | | dependent



### 4.1 Identifying Entities and Attributes
The first thing that should be identified are the entities and their corresponding attributes. Recall that entities are the real-world objects of a database and attributes are are the properties that describe an entity.

*Table 4.2: The entities and attributes of the example presented in Table 4.1*

Entities | Entity Attributes
-------- | ------------------
DEPARTMENT | Name, ID, Manager, Manager_start_date, Location
PROJECT | Name, Number, Location, Hours
EMPLOYEE | Name, SSN, Address, Salary, Gender, Birthdate, Supervisor
DEPENDENT | First name, Gender, Birthdate, Relationship

### 4.2 Identifying Types of Entities and Attributes 
After the entities and all of their attributes are identified we need to identify the types of each entity and attributes, and determine what the keys of each entity are. To start we will identify if an entity is a strong or weak. For this example all entities are strong except DEPENDENT. We know this because each DEPENDENT has to have an EMPLOYEE. Given what we know about weak entities we can determine two things about the DEPENDENT entity. The first is that there is a DEPENDS_ON relationship between EMPLOYEE and DEPENDENT that has a cardinality of one to many. The second is that DEPENDENT needs to have a partial key. 

Next we will identify the types of attributes, including whether or not an attribute is a key. When determining different types of attributes it is important to pay close attention to the language used to describe them. For example in Table 4.1, DEPARTMENT Name and ID are both described as unique. This means that both DEPARTMENT Name and ID are candidate keys. The same is assumed of PROJECT Name and Number. Identifying the keys for EMPLOYEE and DEPENDENT is a little more tricky. When there is not any specific language used identifying unique attributes, you will need to determine which of the attributes listed for the entity could be considered a unique value. For EMPLOYEE the only attribute listed that would be guaranteed to be unique would be SSN, therefore SSN is the primary key for EMPLOYEE. The only attribute for DEPENDENT that could be a unique value is Name. Because DEPENDENT is a weak entity, Name would be a partial key.

Now that the keys for each entity have been identified we need to examine the other attributes and determine what type of attributes they are. Looking back on Table 4.1 we know that a DEPARTMENT can have many locations. This means that DEPARTMENT Location is a multi-valued attribute. Location for PROJECT on the other hand is specified as a single-valued attribute because a project can only have one Location, so it will be represented with a single oval. Given the information gathered from other entities and attributes we can determine the number of employees that work for each department. Therefore, Number_of_employees is a derived attribute of DEPARTMENT and is represented as an oval with a dotted line. All other attributes in this database are considered simple attributes except EMPLOYEE Name which is a composite attribute because it can be broken into first name, middle initial, and last name.

### 4.3 Identifying Relationships
The next step is to identify the relationships between entities. We have already identified one between EMPLOYEE and the weak entity DEPENDENT. Looking back at the assumptions presented in Table 4.1 we can infer the relationships described in the table below.

*Table 4.3: Relationships*  

| Entity     | Relationship  | Entity    |  
|------------|---------------|-----------|  
| EMPLOYEE   | WORKS_FOR     | DEPARTMENT|  
| EMPLOYEE   | MANAGES       | DEPARTMENT|  
| EMPLOYEE   | SUPERVISES    | EMPLOYEE  |  
| DEPARTMENT | CONTROLS      | PROJECT   |  
| EMPLOYEE   | WORKS_ON      | PROJECT   |  
| EMPLOYEE   | DEPENDENTS_OF | DEPENDENT |  


After identifying the relationships between entities the next step is to identify the participation and the cardinality of each relationship. Looking at the first relationship from the Table 4.3 and the assumptions from Table 4.1, each employee is a part of only one department. This means there is a many-to-one cardinality. We also know that each employee has to be a part of a department and each department must have at least one employee. This is an example of total participation and is represented with double lines connecting the entities to the relationship, which is shown in Figure 4.1 below. The MANAGES relationship has a one to one cardinality because each employee can only manage one department and each department can only have one manager. This relationship has total participation between the DEPARTMENT entity, because a department must have a manager, but there is not total participation between the EMPLOYEE entity. A manager must also be an employee, so the employee entity is a part of the Manages relationship, but there is not total participation because an employee is not necessarily a manager. The cardinality and participation of each relationship can be determined using the logic we used in the examples above, and can be see in the picture of the final diagram. 

### 4.4 Putting It All Together
After identifying the relationships in the database, we are ready to combine all the information into a visual representation. The ER diagram of the company example described in Table 4.1 is shown in Figure 4.1.

![Figure 4.1](images/14.png)  
*Figure 4.1*

You will notice that there are some components of this diagram that look a little different or were not mentioned in the database description. The first is that the DEPARTMENT attribute Manager_start_date is not represented as an attribute of the DEPARTMENT entity but as the MANAGES relationship, and a similar situation occurs with the hours attribute from the PROJECT entity. One thing that needs to be considered when drawing an ER diagram is if it makes the most sense for an attribute to be an entity attribute or an attribute of a relationship. Another difference is the names of the database components. In an ER diagram, entities and relationships are represented with all capital letters, and attributes are represented with the first letter capitalized and the rest of the name lower case by convention. An ER diagram also uses underscores to represent spaces. When the names of attributes or entities are too long, it is easy for a diagram to become cluttered and difficult to read, so it is important to keep the names of the elements in a diagram short. It is also important that the names are concise to help the readability of the diagram. For example, if EMPLOYEE SSN was called Number instead, then a person referencing this diagram may become confused about the meaning of that attribute. Number is a vague description; it could mean a number given by the company, a phone number, or the number of dependents an employee has. SSN is a better description of what the attribute represents and will help limit any confusion about that attribute. 